var bodyContent = context.getVariable('request.content');
var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);

var body = JSON.parse(bodyContent);
context.setVariable("isValidRequest", "true"); 
context.setVariable("correlationId", body.correlationId);

for (var index = 0 ; index < body.billingAccounts.length ; index ++ ) {
    if (typeof body.billingAccounts[index].email != 'undefined' && !regex.test(body.billingAccounts[index].email)) {
        context.setVariable("isValidRequest", "false");
        context.setVariable("errorMessage", "Please provide a valid email");
        break;
    } else if(!body.billingAccounts[index].addresses.some(address => address.type === "MAILING") || !body.billingAccounts[index].addresses.some(address => address.type === "SERVICE")) {
        context.setVariable("isValidRequest", "false");
        context.setVariable("errorMessage",  "Missing SERVICE/MAILING address type");
        break;
    }
}
